terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "node1-rg" {
  name     = "node1"
  location = "eastus"
}

resource "azurerm_public_ip" "pu-node1" {
  name		       = "node1"
  resource_group_name = azurerm_resource_group.node1-rg.name
  location            = azurerm_resource_group.node1-rg.location
  allocation_method   = "Static"
}

resource "azurerm_virtual_network" "vn-node1" {
  name                = "node1"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.node1-rg.location
  resource_group_name = azurerm_resource_group.node1-rg.name
}

resource "azurerm_subnet" "s-node1" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.node1-rg.name
  virtual_network_name = azurerm_virtual_network.vn-node1.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "nic-node1" {
  name                = "example-nic"
  location            = azurerm_resource_group.node1-rg.location
  resource_group_name = azurerm_resource_group.node1-rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.s-node1.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.pu-node1.id

  }
}

resource "azurerm_linux_virtual_machine" "vm-node1" {
  name                = "node1"
  resource_group_name = azurerm_resource_group.node1-rg.name
  location            = azurerm_resource_group.node1-rg.location
  size                = "Standard_B1s"
  admin_username      = "toto"
  network_interface_ids = [
    azurerm_network_interface.nic-node1.id,
  ]

  admin_ssh_key {
    username   = "toto"
    public_key = file("id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }
}


resource "azurerm_resource_group" "node2-rg" {
  name     = "node2"
  location = "eastus"
}

resource "azurerm_virtual_network" "vn-node2" {
  name                = "node2"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.node2-rg.location
  resource_group_name = azurerm_resource_group.node2-rg.name
}


resource "azurerm_subnet" "s-node2" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.node2-rg.name
  virtual_network_name = azurerm_virtual_network.vn-node2.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "nic-node2" {
  name                = "example-nic"
  location            = azurerm_resource_group.node2-rg.location
  resource_group_name = azurerm_resource_group.node2-rg.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.s-node2.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "vm-node2" {
  name                = "node2"
  resource_group_name = azurerm_resource_group.node2-rg.name
  location            = azurerm_resource_group.node2-rg.location
  size                = "Standard_B1s"
  admin_username      = "toto"
  network_interface_ids = [
    azurerm_network_interface.nic-node2.id,
  ]

  admin_ssh_key {
    username   = "toto"
    public_key = file("id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "erockyenterprisesoftwarefoundationinc1653071250513"
    offer     = "rockylinux-9"
    sku       = "rockylinux-9"
    version   = "9.1.20230215"
  }
}