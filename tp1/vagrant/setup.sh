dnf update -y

dnf install -y ansible

dnf install -y python

dnf install -y nano

setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config

useradd test

passwd -f -u test

echo "test ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

dnf -y install openssh-server

systemctl start sshd

systemctl enable sshd

firewall-cmd --add-service=ssh --permanent

mkdir /home/test/.ssh

touch /home/test/.ssh/authorized_keys

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC2Ixl8opP9CmTu53Dhdx0E65KvFcpHs34R2qxsNolB7mrENHx5QWY/fYllHWrqO7Bq2auGPSHTV5r4mFYmeAyZLK/WHEDa0ZUj39PfemP2aB1ONXJilN4j0FRvfBQRRbRPdq3eEHhBetv0UgX3oV6VGPzBK9t5EJmOlWl7lYi3qNASS3INBd6YE40Xj9N2LTjrVCzpxbvqqPT8K15XjwVyyZP/UAG8TsStkfgczwfiGyU2nL4xkvraVY8NL3S3VAr79jS2VX8R54aCYEJeDTATB02+kZL/Jkijz87UtxC+ecbI/VNVaPmv7FHYFwW+X83m5T3wud3/DmPdG8zwgF6+VbLwr84GXFdsJFJNsrUmCj3tcQ4KWguha0LRjdot8yb4gLXGOYTb42wOIWbbkPBglVqkAcNbN6VKKjZykOih7o2qjMJPYVxtdnOWu7HPk88Mcz8rj3zi/3ACOD22mHltlw/G0Kq5n1+g7g6HtLt+/kct/+JQbxptDLLzTOf6ISuTsnLFiu5NT8tKI+abQM3RzV6tFh//EphTiPXNUzA5x1OSdU+nER48+WCmkiw0onROQONxXdQJu98S1uJSfMp36AMCYCnaoaH79Gsth+ByZM0FVJYeg6aJUTEp0rmxwzhYD+KPRaIsUAoIzHloTXVuHSKWuZhsVHLSiL7kDYcWjQ== toanu@MSI" >> /home/test/.ssh/authorized_keys

chmod -R go= ~/.ssh

chmod 700 /home/test/.ssh/
chmod 600 /home/test/.ssh/authorized_keys

chown -R test:test /home/test/.ssh
